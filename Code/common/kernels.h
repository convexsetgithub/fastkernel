#ifndef KERNELS_H
#define KERNELS_H
#include <shark/LinAlg/Base.h>
#include <math.h>
#include <fastkernelexceptions.h>

namespace common{
namespace kernels{

///
/// \brief RBF
/// \param x
/// \param y
/// \return
/// The RBF kernel value using sigma=1
double RBF(shark::RealVector& x, shark::RealVector& y);
double Polynomial2(shark::RealVector& x, shark::RealVector& y);

}
}

#endif // KERNELS_H
