#ifndef RESIZER_H
#define RESIZER_H
#include <shark/LinAlg/Base.h>

class resizer
{
public:
    resizer(unsigned size_) : m_size(size_){}

    typedef shark::RealVector result_type;

    shark::RealVector operator()(shark::RealVector input) const {
        input.resize(m_size);
        return input;
    }
private:
    double m_size;
};

#endif // RESIZER_H
