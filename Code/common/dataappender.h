#ifndef DATAAPPENDER_H
#define DATAAPPENDER_H
#include <shark/LinAlg/Base.h>

class dataappender
{
public:
    dataappender(unsigned int d_,unsigned int D_);

    typedef shark::RealVector result_type;

    shark::RealVector operator() (shark::RealVector input) const{
        if(input.size()!=m_dimension){
            shark::RealVector correctsize;
            correctsize.reserve(m_dimension);
            for(unsigned int i=0;i<m_dimension;++i){
                if(i<input.size())
                    correctsize[i]=input[i];
                else
                    correctsize[i]=m_append;
            }
        }
        return input;
    }
private:
    unsigned int m_dimension;
    double m_append;
};

#endif // DATAAPPENDER_H
