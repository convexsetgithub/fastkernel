#include "hadamardmatrix.h"
#include <shark/LinAlg/Base.h>
#include <exception>
#include <vector>
#include <math.h>
#include <timer.h>
#include <Common.h>
#include <iterator>
#include<shark/Data/Dataset.h>


using namespace shark;



HadamardMatrix::HadamardMatrix()
{
}

void HadamardMatrix::populate_n(const unsigned int d){
    m_matrix = shark::RealMatrix(d,d,0);
    double factor = std::pow(d,-0.5);
    for(unsigned int i=0;i<d;++i){
        for(unsigned int j=0;j<d;++j){
            int c=0;
            int v=(i & j);
            for (c; v; v >>= 1)
            {
              c += v & 1;
            }
            if(c % 2==0)
            {
                m_matrix(i,j)=factor;
            }
            if(c % 2==1)
            {
                m_matrix(i,j)=-factor;
            }
        }
    }

}

void HadamardMatrix::populate(const unsigned int d){
    m_matrix = shark::RealMatrix(d,d,0);
    double factor = 1;
    for(unsigned int i=0;i<d;++i){
        for(unsigned int j=0;j<d;++j){
            int c=0;
            int v=(i & j);
            for (c; v; v >>= 1)
            {
              c += v & 1;
            }
            if(c % 2==0)
            {
                m_matrix(i,j)=factor;
            }
            if(c % 2==1)
            {
                m_matrix(i,j)=-factor;
            }
        }
    }

}

RealMatrix HadamardMatrix::matrix(){
    return m_matrix;
}


// Fast in-place Walsh-Hadamard Transform
void inline HadamardMatrix::wht_bfly (double& a, double& b) const
{
        double tmp = a;
        a += b;
        b = tmp - b;
}

//Integer log2
int inline HadamardMatrix::log2 (int x) const
{
        int l2;
        for (l2 = 0; x > 0; x >>=1)
        {
                ++ l2;
        }

        return (l2);
}

void HadamardMatrix::FWHT (std::vector<double>& data) const
{
  int s = data.size();
  if(!common::powerOfTwo(data.size())) throw FastKernelExceptions::PowerOfTwoException();
  const int power = log2 (data.size()) - 1;
  for (int i = 0; i < power; ++i)
  {
    for (int j = 0; j < (1 << power); j += 1 << (i+1))
    {
       for (int k = 0; k < (1<<i); ++k)
       {
           wht_bfly (data [j + k], data [j + k + (1<<i)]);
       }
    }
  }
}


void HadamardMatrix::FWHT (RealVector& data) const
{
  int s = data.size();
  if(!common::powerOfTwo(data.size())) throw FastKernelExceptions::PowerOfTwoException();
  const int power = log2 (data.size()) - 1;
  for (int i = 0; i < power; ++i)
  {
    for (int j = 0; j < (1 << power); j += 1 << (i+1))
    {
       for (int k = 0; k < (1<<i); ++k)
       {
           wht_bfly (data [j + k], data [j + k + (1<<i)]);
       }
    }
  }
}

void HadamardMatrix::FWHTn (std::vector<double>& data) const
{
    HadamardMatrix::FWHT(data);
    double factor = std::pow((double)data.size(),-0.5);
    for (int i=0;i<data.size();++i){
        data[i]=data[i]*factor;
    }
}

void HadamardMatrix::FWHTn (RealVector& data) const
{
    HadamardMatrix::FWHT(data);
    double factor = std::pow((double)data.size(),-0.5);
    for (int i=0;i<data.size();++i){
        data[i]=data[i]*factor;
    }
}


