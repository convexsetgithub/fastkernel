#include "uniform_distribution.h"

Uniform_distribution::Uniform_distribution(const double from_, const double to_, const int dimensions_, const double sparse_)
    : m_uni_dist(from_,to_), m_dimensions(dimensions_),m_mt(std::random_device()()),m_sparse(to_*sparse_)
{
}

Uniform_distribution::~Uniform_distribution(){
}

void Uniform_distribution::draw(shark::RealVector &input) const{
    input.resize(m_dimensions);
    for(int i=0;i<m_dimensions;++i){
        if(std::abs(m_uni_dist(m_mt))>=m_sparse){
            input(i)=m_uni_dist(m_mt);
        }
        else input(i)=0;
    }
}
