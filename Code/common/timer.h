#ifndef TIMER_H
#define TIMER_H
#include <chrono>

namespace common{
class Timer
{
public:
    Timer();
    ~Timer();
    void start();
    void stop();
    void reset();
    long int hours();
    long int minutes();
    long long int seconds();
    long long int milliseconds();
    long long int microseconds();
    long long int nanoseconds();
private:
    std::chrono::high_resolution_clock::time_point m_start = std::chrono::high_resolution_clock::now();
    std::chrono::high_resolution_clock::time_point m_end;
    bool m_is_running=0;
};
}

#endif // TIMER_H
