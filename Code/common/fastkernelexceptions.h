#ifndef FASTKERNELEXCEPTIONS_H
#define FASTKERNELEXCEPTIONS_H
#include <exception>

namespace FastKernelExceptions{

//A power of two matrix can be interpreted as a 2x2x2...x2x2 multidimensional DFT
class PowerOfTwoException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "The dimension used must be power of two";
    }
};

//Dimensions didn't match
class DimensionException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Dimensions didn't match";
    }
};

//D<d not supported in FastFood
class FastFoodFeatureSizeException: public std::exception
{
    virtual const char* what() const throw()
    {
        return "Number of features has to be greater than number of input dimensions for this FastFood implementation. Otherwise append 0 inputs to d.";
    }
};


}

#endif // FASTKERNELEXCEPTIONS_H
