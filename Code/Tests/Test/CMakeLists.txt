project(Test)
cmake_minimum_required(VERSION 2.8)
aux_source_directory(. SRC_LIST)
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++11")

include_directories("${Test_SOURCE_DIR}/../../common")
add_subdirectory("${Test_SOURCE_DIR}/../../common" "${Test_SOURCE_DIR}/common")
include_directories("${Test_SOURCE_DIR}/../../FastFood")
add_subdirectory("${Test_SOURCE_DIR}/../../FastFood" "${Test_SOURCE_DIR}/FastFood")
include_directories("${Test_SOURCE_DIR}/../../TensorSketch")
add_subdirectory("${Test_SOURCE_DIR}/../../TensorSketch" "${Test_SOURCE_DIR}/TensorSketch")

option (USE_FFTW "Use FFTW for Fourier transform" ON)

if(USE_FFTW)
    include_directories("${Test_SOURCE_DIR}/../../fftw++-1.13")
    add_subdirectory("${Test_SOURCE_DIR}/../../fftw++-1.13" "${Test_SOURCE_DIR}/FFTW")
    set (FFTW_LIBS ${FFTW_LIBS} fftw++)
endif(USE_FFTW)

set (VT_LIBS ${VT_LIBS} common)

add_executable(${PROJECT_NAME} ${SRC_LIST}
${Test_SOURCE_DIR}/../../common/Common.cpp
${Test_SOURCE_DIR}/../../common/dataconverter.cpp
${Test_SOURCE_DIR}/../../common/hadamardmatrix.cpp
${Test_SOURCE_DIR}/../../common/normal_distribution.cpp
${Test_SOURCE_DIR}/../../common/fastkernelexceptions.cpp
${Test_SOURCE_DIR}/../../FastFood/fastfood.cpp
${Test_SOURCE_DIR}/../../FastFood/stacker.cpp
${Test_SOURCE_DIR}/../../FastFood/GaussSampler.cpp
${Test_SOURCE_DIR}/../../TensorSketch/TensorSketch.cpp
${Test_SOURCE_DIR}/../../TensorSketch/CountSketch.cpp
${Test_SOURCE_DIR}/../../fftw++-1.13/fftw++.cc
)

target_link_libraries(Test fftw3 fftw3_omp gomp boost_serialization boost_system boost_filesystem boost_program_options boost_unit_test_framework)
