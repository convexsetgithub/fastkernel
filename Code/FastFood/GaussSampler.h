#ifndef GAUSSSAMPLER_H
#define GAUSSSAMPLER_H
#include <shark/LinAlg/Base.h>
#include <random>
#include <vector>
#include <math.h>

class GaussSampler{
public:
  GaussSampler(const double mean, const double stdiv);
  ~GaussSampler();
  shark::RealMatrix gaussMatrix(const unsigned int d, const unsigned int D);
  std::vector<double> gaussSquareDiagonal(const unsigned int d);
  std::vector<double> gaussSquareDiagonal_abs(const unsigned int d);
  double sample();
  std::mt19937& engine() {return mt;}
private:
  std::mt19937 mt;
  double mean;
  double stdiv;
  std::normal_distribution<> gauss_dist;
};
#endif
