#ifndef FASTFOOD_H
#define FASTFOOD_H
#define _USE_MATH_DEFINES
#include <random>
#include <vector>
#include <Common.h>
#include <GaussSampler.h>
#include <shark/LinAlg/Base.h>
#include <datatransform.h>
#include <math.h>
#include <fastkernelexceptions.h>
#include <iomanip>

class FastFood : public common::Datatransform
{
public:
    FastFood(unsigned int,double sigma_=1);
    FastFood(vector<double> S_, vector<double> G_, vector<int> B_ , int PIseed_);

    void signScramble(vector<double>&) const;
    void signScramble(shark::RealVector&) const;

    void permutate(vector<double>&) const;
    void permutate(shark::RealVector&) const;

    void gaussDiagonal(vector<double>&) const;
    void gaussDiagonal(shark::RealVector&) const;

    void scalingDiagonal(vector<double>& data) const;
    void scalingDiagonal(shark::RealVector& data) const;

    void vProduct(vector<double>& data) const;
    void vProduct(shark::RealVector& data) const;

    typedef shark::RealVector result_type;

    shark::RealVector operator() (shark::RealVector input) const{
        this->vProduct(input);
        return input;
    }

    const unsigned int dimension() {return m_dimension;}

    void print_operators();
private:
    void normalizationScaling();
    void radialGaussianScaling();
    unsigned int m_dimension;
    vector<int> m_B;
    vector<double> m_G;
    vector<double> m_S;
    double m_sigma_inverse;
    double m_FrobG;
    unsigned int m_seed;
    std::uniform_int_distribution<int> m_uni_int_dist;
    mutable GaussSampler m_gauss;
    HadamardMatrix m_H;
};

#endif // FASTFOOD_H

