#ifndef TS_MAP_H
#define TS_MAP_H
#include <vector>
#include <Common.h>
#include <fftw++.h>
#include <CountSketch.h>
#include <vector>
#include <fastkernelexceptions.h>
#include <map>



class TensorSketch{
 public:
  TensorSketch(const unsigned int d_, const unsigned int D_, const unsigned int p_, const double C_=0);
  TensorSketch(const unsigned int d_, const unsigned int D_, const unsigned int p_, vector<CountSketch<shark::RealVector>> &CS_array,const double C_=0);

  shark::RealVector map(shark::RealVector& in) const;

  typedef shark::RealVector result_type;
  shark::RealVector operator() (shark::RealVector input) const{
      input = this->map(input);
      return input;
  }

  unsigned int features();
 private:
  const unsigned int m_d;
  const unsigned int m_D;
  const unsigned int m_p;
  const double m_sqrt_C;
  std::vector <double*> m_f;
  std::vector <complex<double>*> m_g;
  shared_ptr<fftwpp::rcfft1d> m_FFT_Forward;
  shared_ptr<fftwpp::crfft1d> m_FFT_Backward;
  mutable vector<CountSketch<shark::RealVector>> m_CS;
};

#endif
