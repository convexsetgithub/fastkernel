#ifndef COUNTSKETCH_H
#define COUNTSKETCH_H
#include <map>
#include <random>
#include <iostream>
#include <string>
#include <numeric>
#include <fastkernelexceptions.h>
#include <shark/LinAlg/Base.h>

#include <Common.h>

template <class T>
class CountSketch{
public:
  CountSketch(const unsigned int d_,const unsigned int D_);
  CountSketch(const unsigned int d_, const unsigned int D_,std::map<unsigned int, unsigned int> h_, std::map<unsigned int, signed int> s_);
  ~CountSketch();
  T sketchVector (const T& point) const;
  void reseed(unsigned int d_, unsigned int D_);
  void reseed();
  T get_cs();
  std::map<unsigned int, unsigned int> getHash_map();
  std::map<unsigned int, signed int> getBool_map();
  void print_hashmap();
  void print_boolmap();
private:
  std::map<unsigned int, unsigned int> hash_map(int input_range , int Desired_Dimension);
  std::map<unsigned int,int> bool_map(int input_range);
  mutable std::map<unsigned int,unsigned int> m_h;
  mutable std::map<unsigned int,int> m_s;
  unsigned int m_d;
  unsigned int m_D;
  mutable T m_cs;
};

template <class T>
CountSketch<T>::CountSketch(const unsigned int d_,const unsigned int D_) :
  m_d(d_), m_D(D_)
{
  reseed(m_d,m_D);
}

template <class T>
CountSketch<T>::CountSketch(const unsigned int d_,const unsigned int D_,std::map<unsigned int,unsigned int> h_, std::map<unsigned int,int> s_) :
    m_d(d_), m_D(D_),m_h(h_),m_s(s_)
{

}

template <class T>
CountSketch<T>::~CountSketch()
{
}

template<class T>
void CountSketch<T>::print_hashmap(){
    for(auto it = m_h.cbegin(); it != m_h.cend(); ++it)
    {
        std::cout << it->first << " : " << it->second << "\n";
    }
}
template<class T>
void CountSketch<T>::print_boolmap(){
    for(auto it = m_s.cbegin(); it != m_s.cend(); ++it)
    {
        std::cout << it->first << " : " << it->second << "\n";
    }
}


template <class T>
void CountSketch<T>::reseed(unsigned int d, unsigned int D)
{
  this->m_d = d;
  this->m_D = D;
  m_h = hash_map(d,D);
  m_s = bool_map(d);
}

template <class T>
void CountSketch<T>::reseed()
{
 m_h = hash_map(m_d,m_D);
 m_s = bool_map(m_d);
}

template <class T>
std::map<unsigned int,unsigned int> CountSketch<T>::hash_map(int d_, int D_){
    std::random_device rd;
    std::map<unsigned int,unsigned int> mapping;
    std::uniform_int_distribution<int> dist(0,D_-1);
    for( int i = 0; i< d_;i++)
      {
        mapping[i]=dist(rd);
      }
    return mapping;
}

template <class T>
std::map<unsigned int,int> CountSketch<T>::bool_map(int d_){
    std::random_device rd;
    std::map<unsigned int,int> mapping;
    std::uniform_int_distribution<int> dist(0,1);
    int pm [2] = {-1,1};
    for( int n = 0; n< d_;n++)
      {
      mapping[n]=pm[dist(rd)];
      }
    return mapping;
}

template <class T>
std::map<unsigned int, unsigned int> CountSketch<T>::getHash_map()
{
  return m_h;
}

template <class T>
std::map<unsigned int, signed int> CountSketch<T>::getBool_map()
{
  return m_s;
}





template <class T>
T CountSketch<T>::sketchVector(const T &point) const
{
  if(point.size()!=m_d)
      throw FastKernelExceptions::DimensionException();

  m_cs = T(m_D,0.0);

  for (unsigned int j=0;j<m_d;j++)
    {
      m_cs[m_h[j]]+=point[j]*m_s[j];
    }

  return m_cs;
}

template <class T> T CountSketch<T>::get_cs()
{
  return m_cs;
}




#endif
