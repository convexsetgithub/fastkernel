# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('Experiments/difference.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

axislabels = [16,32,64,128,256,512,1024]
fastfood = [np.mean([abs(float(x[5]))/float(x[3]) for x in data if (x[0]=='FastFood Diff' and int(x[2])==y and float(x[3])!=0)])*100 for y in axislabels]
tensorSketch = [np.mean([abs(float(x[5]))/float(x[3]) for x in data if (x[0]=='TS Diff' and int(x[2])==y and float(x[3])!=0)])*100 for y in axislabels]



fig, ax = plt.subplots()
ax.plot(axislabels, fastfood, 'b^', label='FastFood Gaussian RBF')
ax.plot(axislabels, fastfood, 'k--', label='_nolegend_')
ax.plot(axislabels, tensorSketch, 'r*', label='TensorSketch Gaussian RBF')
ax.plot(axislabels, tensorSketch, 'k--', label='_nolegend_')

#ax.set_ylim(-1, 100)
ax.set_xlim(16, 1024)
ax.set_xticks([16,32,64,128,256,512,1024])


#plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.legend(numpoints=1,loc=1)
plt.xlabel('Number of features.$D$')
plt.ylabel('Average relative error,$\%$')

plt.savefig('FFTSDiffError.png', bbox_inches=0,dpi=300)

