# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('Experiments/1FastFood_8_128.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

realkernel = [float(x[3]) for x in data]
diff = [abs(float(x[5])) for x in data]
kernelsteps = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]

matrix = [[abs(float(x[5])) for x in data if kernelsteps[y] <= float(x[3]) and float(x[3]) < kernelsteps[y+1]] for y in range(0,10)]

fig, ax = plt.subplots()
ax.boxplot(matrix)
ax.set_xticklabels([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1])


#plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.xlabel('Kernel value,$\Delta$=0.1')
plt.ylabel('Absolute Error distribution')

plt.savefig('FFboxplotTMP.png', bbox_inches=0,dpi=300)

