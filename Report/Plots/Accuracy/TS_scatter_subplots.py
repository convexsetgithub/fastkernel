# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

with open('Experiments/0TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'_.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel0 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast0 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]


with open('Experiments/1TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'_.txt', 'Ur') as f:

    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel1 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast1 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

with open('Experiments/2TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'_.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel2 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast2 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]
with open('Experiments/3TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'_.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel3 = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast3 = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]

f, axarr = plt.subplots(2,2)
axarr[0, 0].scatter(kernel0,fast0,s=2,alpha=0.1)
axarr[0, 1].scatter(kernel1,fast1,s=2,alpha=0.1)
axarr[1, 0].scatter(kernel2,fast2,s=2,alpha=0.1)
axarr[1, 1].scatter(kernel3,fast3,s=2,alpha=0.1)

#plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
#plt.setp([a.get_yticklabels() for a in axarr[:, 1]], visible=False)

plt.savefig('TS_Scatter_subplots'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.png', bbox_inches=0,dpi=300)

