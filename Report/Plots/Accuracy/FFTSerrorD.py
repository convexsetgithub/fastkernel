# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('FFTSerrorD.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

axislabels = [float(x[1]) for x in data]
fastfood = [float(x[2]) for x in data]
tensorSketch = [float(x[3]) for x in data]



fig, ax = plt.subplots()
ax.plot(axislabels, fastfood, 'b^', label='FastFood RBF')
ax.plot(axislabels, fastfood, 'k--', label='_nolegend_')
ax.plot(axislabels, tensorSketch, 'r*', label='TensorSketch 2-deg')
ax.plot(axislabels, tensorSketch, 'k--', label='_nolegend_')


ax.set_xlim(0, 4096)
ax.set_xticks([256,512,1024,2048,4096])


#plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.legend(numpoints=1,loc=1)
plt.xlabel('Number of features.$D$')
plt.ylabel('Average relative error,$\%$')

plt.savefig('FFTSerrorD.png', bbox_inches=0,dpi=300)

