# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

with open('Experiments/1FastFood_'+str(sys.argv[1])+'_32.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel0 = [float(x[3]) for x in data if x[0]== 'FastFood' ]
fast0 = [float(x[4]) for x in data if x[0]== 'FastFood' ]


with open('Experiments/1FastFood_'+str(sys.argv[1])+'_256.txt', 'Ur') as f:

    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel1 = [float(x[3]) for x in data if x[0]== 'FastFood' ]
fast1 = [float(x[4]) for x in data if x[0]== 'FastFood' ]

with open('Experiments/2FastFood_'+str(sys.argv[1])+'_1024.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel2 = [float(x[3]) for x in data if x[0]== 'FastFood' ]
fast2 = [float(x[4]) for x in data if x[0]== 'FastFood' ]

f, axarr = plt.subplots(1,3)
axarr[0].scatter(kernel0,fast0,s=2,alpha=0.1)
axarr[0].set_title('$D=32$')
axarr[1].scatter(kernel1,fast1,s=2,alpha=0.1)
axarr[1].set_title('$D=256$')
axarr[2].scatter(kernel2,fast2,s=2,alpha=0.1)
axarr[2].set_title('$D=1024$')

#plt.setp([a.get_xticklabels() for a in axarr[0, :]], visible=False)
plt.setp([a.set_ylim(0.3,1) for a in axarr])
plt.setp([a.set_xlim(0.3,1) for a in axarr])
plt.setp([a.set_xticks([0.3,0.5,0.7,0.9]) for a in axarr])
plt.setp(axarr[1].get_yticklabels(), visible=False)
plt.setp(axarr[2].get_yticklabels(), visible=False)

plt.savefig('FF_Scatter_progress.png', bbox_inches=0,dpi=300)

