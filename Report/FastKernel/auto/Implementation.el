(TeX-add-style-hook "Implementation"
 (lambda ()
    (LaTeX-add-labels
     "fig:codebase")
    (TeX-run-style-hooks
     "TestingProb")))

