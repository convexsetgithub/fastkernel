(TeX-add-style-hook "TheKernelTrick"
 (lambda ()
    (LaTeX-add-labels
     "eq: Linear Model"
     "ill:KernelTrick"
     "eq:Kernel expansion"
     "eq:Kernel trick"
     "eq:decision function kernel"
     "def:RBF"
     "def:PolyKernel"
     "head:conversion"
     "eq:gaussPoly")))

