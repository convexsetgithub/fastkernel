(TeX-add-style-hook "CountSketch"
 (lambda ()
    (LaTeX-add-labels
     "eq:CountSketch"
     "ill:CountSketch"
     "lem:CountSketchE"
     "eq:CountSketchVar"
     "eq:countsketch poly"
     "eq:polyform")))

