(TeX-add-style-hook "speed"
 (lambda ()
    (LaTeX-add-labels
     "section:speed"
     "tab:speed"
     "tab:speedcostsofn"
     "fig:speed_n"
     "tab:speedcostsofD"
     "fig:speed_D"
     "tab:speedcostsofd"
     "fig:speed_d")))

