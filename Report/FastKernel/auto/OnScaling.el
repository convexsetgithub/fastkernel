(TeX-add-style-hook "OnScaling"
 (lambda ()
    (LaTeX-add-labels
     "eq:convergence"
     "eq:Bochner"
     "eq:Rahimi07Mapping"
     "eq:eulersmapping"
     "eq:mapbridge")))

