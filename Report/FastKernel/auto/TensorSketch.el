(TeX-add-style-hook "TensorSketch"
 (lambda ()
    (LaTeX-add-labels
     "lem:TensorMap"
     "lem:TensorConstant"
     "eq:FFTMethod"
     "header:TSspeed"
     "TS:errorBound"
     "eq:TensorSketchVar")
    (TeX-run-style-hooks
     "CountSketch")))

