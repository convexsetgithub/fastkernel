(TeX-add-style-hook "FastFood"
 (lambda ()
    (LaTeX-add-labels
     "eq:HadamardExample"
     "eq:Gprime"
     "eq:Length"
     "fig:SquaresToZero"
     "eq:LeDistribution"
     "eq:normalize"
     "eq:StackV"
     "ill:BuildV"
     "header:FFspeed"
     "eq:FastfoodMap"
     "FF:errorBound")))

