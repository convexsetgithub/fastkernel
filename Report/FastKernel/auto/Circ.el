(TeX-add-style-hook "Circ"
 (lambda ()
    (TeX-add-symbols
     '("cmd" 1)
     '("cs" 1)
     '("pkg" 1)
     '("mail" 1))
    (TeX-run-style-hooks
     "hyperref"
     "microtype"
     "textcomp"
     "fontenc"
     "T1"
     "lmodern"
     "xspace"
     "etex"
     "fixltx2e"
     "latex2e"
     "scrartcl10"
     "scrartcl")))

